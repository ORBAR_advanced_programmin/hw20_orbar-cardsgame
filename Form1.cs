﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
namespace HW20_ORBAR
{
    public partial class Form1 : Form
    {
        //Global Variabels
        public static TcpClient client;
        public static NetworkStream clientStream;
        public static IPEndPoint serverEndPoint;
        public static byte[] buffer;
        public static byte[] bufferIn;
        public static int bytesRead;
        public static string input;
        public static int cardNumberPressed = -1;
        public static string cardPressedKind = "";
        public static int opponentNumber = -1;
        public static string opponentPressedKind = "";
        public Form1()
        {
            
            InitializeComponent();
            startGame();
        }
        public void startGame()
        {
            //load main card
            pictureBox11.Image = Image.FromFile(@"C:\PNG-cards\card back blue.png");
            //freeze all buttons
            Enabled = false;
            Thread t = new Thread(serverContact);
            t.Start();
        }
        public void GenerateCards()
        {
            pictureBox1.Image = Image.FromFile(@"C:\PNG-cards\card back red.png");
            pictureBox2.Image = Image.FromFile(@"C:\PNG-cards\card back red.png");
            pictureBox3.Image = Image.FromFile(@"C:\PNG-cards\card back red.png");
            pictureBox4.Image = Image.FromFile(@"C:\PNG-cards\card back red.png");
            pictureBox5.Image = Image.FromFile(@"C:\PNG-cards\card back red.png");
            pictureBox6.Image = Image.FromFile(@"C:\PNG-cards\card back red.png");
            pictureBox7.Image = Image.FromFile(@"C:\PNG-cards\card back red.png");
            pictureBox8.Image = Image.FromFile(@"C:\PNG-cards\card back red.png");
            pictureBox9.Image = Image.FromFile(@"C:\PNG-cards\card back red.png");
            pictureBox10.Image = Image.FromFile(@"C:\PNG-cards\card back red.png");
        }
        public void serverContact()
        {
            string path;
            client = new TcpClient();
            serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
            client.Connect(serverEndPoint);
            clientStream = client.GetStream();
            bufferIn = new byte[4];
            while (true)
            {
                if (cardNumberPressed > opponentNumber && opponentNumber != -1)
                {
                    scoreNumber.Text = (Convert.ToInt32(scoreNumber.Text) + 1).ToString();
                }
                else if (cardNumberPressed < opponentNumber && opponentNumber != -1)
                {
                    oppScore.Text = (Convert.ToInt32(oppScore.Text) + 1).ToString();
                }
                bytesRead = clientStream.Read(bufferIn, 0, 4);
                input = new ASCIIEncoding().GetString(bufferIn);
                if (input[0] == '0')
                {
                    //release all buttonn
                    Enabled = true;
                    GenerateCards();
                }
                if (input[0] == '1')
                {
                    opponentNumber = Convert.ToInt32(input.Substring(1, 2));
                    opponentPressedKind = input.Substring(1);
                    MessageBox.Show(opponentPressedKind);
                    opponentPressedKind = opponentPressedKind.Substring(0,2) + "," + opponentPressedKind[2];
                    MessageBox.Show(opponentPressedKind);
                    path = @"C:\PNG-cards\" + getPictureName(opponentPressedKind);
                    pictureBox11.Image = Image.FromFile(path);
                }
                else//input = 2,forfiet
                {
                    MessageBox.Show("Your score: " + scoreNumber.Text + ",Opponent Score: " + oppScore.Text);
                    this.Close();
                }
            }
        }
        public void sendCode1()
        {
            buffer = new ASCIIEncoding().GetBytes("1" + cardPressedKind.Split(',')[0] + cardPressedKind.Split(',')[1]);
            clientStream.Write(buffer, 0, 4);
            clientStream.Flush();
            cardNumberPressed = -1;
            opponentNumber = -1;
        }
        public string GenerateRandomCard()
        {
            Random rnd = new Random();
            string cardLetter = "";
            string cardNumber = "";
            cardNumber = rnd.Next(1, 14).ToString();
            if(Convert.ToInt32(cardNumber) < 10)
                cardNumber = "0" + cardNumber;
            int number = rnd.Next(1, 5);
            if (number == 1)
                cardLetter = "H";
            else if (number == 2)
                cardLetter = "C";
            else if (number == 3)
                cardLetter = "S";
            else//number = 4
                cardLetter = "D";
            return cardNumber + "," + cardLetter;
        }
        public string getPictureName(string cardKind)
        {
            MessageBox.Show(cardKind);
            string cardNumber = cardKind.Split(',')[0];
            string cardLetter = cardKind.Split(',')[1];
            string type = "";
            if (cardNumber == "01")
                cardNumber = "ace";
            else if (cardNumber == "11")
                cardNumber = "jack";
            else if (cardNumber == "12")
                cardNumber = "queen";
            else if (cardNumber == "13")
                cardNumber = "king";
            if (cardLetter == "H")
                type = "hearts";
            else if (cardLetter == "D")
                type = "diamonds";
            else if (cardLetter == "C")
                type = "clubs";
            else if (cardLetter == "S")
                type = "spades";
            if(cardNumber!="10")
                cardNumber = cardNumber.Replace("0", "");
            MessageBox.Show(cardNumber + "_of_" + type + ".png");
            return cardNumber + "_of_" + type + ".png";
        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {   
            cardNumberPressed = 1;
            cardPressedKind = GenerateRandomCard();
            string path = @"C:\PNG-cards\" + getPictureName(cardPressedKind);
            MessageBox.Show(path);
            pictureBox1.Image = Image.FromFile(path);
            sendCode1();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            cardNumberPressed = 2;
            cardPressedKind = GenerateRandomCard();
            string path = @"C:\PNG-cards\" + getPictureName(cardPressedKind);
            MessageBox.Show(path);
            pictureBox2.Image = Image.FromFile(path);
            sendCode1();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            cardNumberPressed = 3;
            cardPressedKind = GenerateRandomCard();
            string path = @"C:\PNG-cards\" + getPictureName(cardPressedKind);
            MessageBox.Show(path);
            pictureBox3.Image = Image.FromFile(path);
            sendCode1();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            cardNumberPressed = 4;
            cardPressedKind = GenerateRandomCard();
            string path = @"C:\PNG-cards\" + getPictureName(cardPressedKind);
            MessageBox.Show(path);
            pictureBox4.Image = Image.FromFile(path);
            sendCode1();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            cardNumberPressed = 5;
            cardPressedKind = GenerateRandomCard();
            string path = @"C:\PNG-cards\" + getPictureName(cardPressedKind);
            MessageBox.Show(path);
            pictureBox5.Image = Image.FromFile(path);
            sendCode1();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            cardNumberPressed = 6;
            cardPressedKind = GenerateRandomCard();
            string path = @"C:\PNG-cards\" + getPictureName(cardPressedKind);
            MessageBox.Show(path);
            pictureBox6.Image = Image.FromFile(path);
            sendCode1();
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            cardNumberPressed = 7;
            cardPressedKind = GenerateRandomCard();
            string path = @"C:\PNG-cards\" + getPictureName(cardPressedKind);
            MessageBox.Show(path);
            pictureBox7.Image = Image.FromFile(path);
            sendCode1();
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            cardNumberPressed = 8;
            cardPressedKind = GenerateRandomCard();
            string path = @"C:\PNG-cards\" + getPictureName(cardPressedKind);
            MessageBox.Show(path);
            pictureBox8.Image = Image.FromFile(path);
            sendCode1();
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            cardNumberPressed = 9;
            cardPressedKind = GenerateRandomCard();
            string path = @"C:\PNG-cards\" + getPictureName(cardPressedKind);
            MessageBox.Show(path);
            pictureBox9.Image = Image.FromFile(path);
            sendCode1();
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            cardNumberPressed = 10;
            cardPressedKind = GenerateRandomCard();
            string path = @"C:\PNG-cards\" + getPictureName(cardPressedKind);
            MessageBox.Show(path);
            pictureBox10.Image = Image.FromFile(path);
            sendCode1();
        }

        private void fButton_Click(object sender, EventArgs e)//forfiet
        {
            buffer = new ASCIIEncoding().GetBytes("2000");
            clientStream.Write(buffer, 0, 4);
            clientStream.Flush();
            MessageBox.Show("Your score: " + scoreNumber.Text + ",Opponent Score: " + oppScore.Text);
            this.Close();
        }
    }
}
